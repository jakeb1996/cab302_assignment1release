/**
 * 
 * This file is part of the VotingWizard Project, written as 
 * part of the assessment for CAB302, Semester 1, 2016. 
 * 
 * TODO: 	in getPreference() should I be checking the candidate index is in range?
 * 			
 * 			invertVote()
 * 
 */
package asgn1Election;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * <p>Implementing class for the {@link asgn1Election.Vote} interface. <code>Vote</code> 
 * should be implemented as some sort of <code>List</code>, with 
 * <code>ArrayList<Integer></code> the default choice.</p>
 * 
 * @author hogan
 * 
 */
public class VoteList implements Vote {
	/** Holds the information that comprises a single vote */
	private List<Integer> vote;

	/** Number of candidates in the election */
	private int numCandidates;

	/**
	 * <p>Simple Constructor for the <code>VoteList</code> class. <code>numCandidates</code> 
	 * is known to be in range through check on <code>VoteCollection</code>. 
	 * 
	 * @param numCandidates <code>int</code> number of candidates competing for 
	 * this seat. 
	 */
	public VoteList(int numCandidates) {
		this.numCandidates = numCandidates;
		this.vote = new ArrayList<Integer>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Vote#addPref(asgn1Election.CandidateIndex)
	 */
	@Override
	public boolean addPref(int index) {
		// check that the number of votes has not exceeded the number of candidates.
		if (vote.size() < numCandidates) {
			vote.add(index); // add vote to list
			return true; // vote successfully added
		}
		return false; // potential votes exhausted, do not add more!
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Vote#copyVote()
	 */
	@Override
	public Vote copyVote() {
		// create a temporary VoteList object
		Vote temp = new VoteList(this.numCandidates);
		
		// copy the preferences
		Iterator<Integer> itr = this.vote.iterator();
		while (itr.hasNext()) {
			Integer intVote = (Integer)itr.next();
			temp.addPref(intVote);
		}
		
		// return the temporary VoteList
		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Vote#getPreference(int)
	 */
	@Override
	public CandidateIndex getPreference(int cand) {
		return new CandidateIndex(this.vote.indexOf(cand) + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Vote#invertVote()
	 */
	@Override
	public Vote invertVote() {
		// create a new votelist to return
		Vote newVote = new VoteList(this.numCandidates);
		
		// for each candidate
		for (int candNum = 0; candNum < this.numCandidates; candNum++) {
			
			newVote.addPref(vote.indexOf(candNum + 1));		

		}
		
		return newVote;
	}

	/* 
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Integer> iterator() {
		return vote.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "";
		for (Integer index : this.vote) {
			str += index.intValue() + " ";
		}
		return str;
	}
}
