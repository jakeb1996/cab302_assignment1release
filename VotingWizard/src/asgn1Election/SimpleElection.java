/**
 * 
 * This file is part of the VotingWizard Project, written as 
 * part of the assessment for CAB302, Semester 1, 2016. 
 * 
 */
package asgn1Election;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

import asgn1Util.Strings;

/**
 * 
 * Subclass of <code>Election</code>, specialised to simple, first past the post voting
 * 
 * @author hogan
 * 
 */
public class SimpleElection extends Election {

	/**
	 * Simple Constructor for <code>SimpleElection</code>, takes name and also sets the 
	 * election type internally. 
	 * 
	 * @param name <code>String</code> containing the Election name
	 */
	public SimpleElection(String name) {
		super(name);
		this.type = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Election#findWinner()
	 */
	@Override
	public String findWinner() {
		
		String output = "";
		output += showResultHeader();
		vc.countPrimaryVotes(this.cds);
		
		output += "Counting primary votes; " + cds.size() + " alternatives available\n";
		output += reportCountResult();
		
		// setup the winner
		Candidate winner;
		
		// see if there is a "clear winner"
		Candidate clearWinner = this.clearWinner(vc.getFormalCount() / 2 + 1);
		
		if (clearWinner != null) {
			winner = clearWinner;
		}
		else {
			// there is not a "clear winner"
			
			/* find the candidate with the most first preference votes */
			// setup the keys for the candidates collection
			Set<CandidateIndex> candidates = cds.keySet();
			
			// setup variables to remember who has the highest vote count
			CandidateIndex winnerIndex = cds.firstKey();
			
			// iterate over all candidates
			for (CandidateIndex cdi : candidates) {
				// if this candidate has more votes than the last most successful
				if (cds.get(cdi).getVoteCount() > cds.get(winnerIndex).getVoteCount()) {
					winnerIndex = cdi;
				}
				
			}
			
			winner = cds.get(winnerIndex);
		}
		
		// return a string identifying the winner
		output += reportWinner(winner);
		
		return output;
	}

	/* 
	 * (non-Javadoc)
	 * @see asgn1Election.Election#isFormal(asgn1Election.Vote)
	 */
	@Override
	public boolean isFormal(Vote v) {
		// parseVoteFromLine() already checks whether the number of integer entries ...
		// on the vote card is the same as the number of candidates
		
		// check whether only one candidate is voted as the first preferred candidate
		boolean first_pref_exists = false;
		Iterator<Integer> test = v.iterator();
		while (test.hasNext()) {
			Integer pref = (Integer)test.next();
			if (pref == 1) {
				if (first_pref_exists == false) {
					first_pref_exists = true;
				}
				else {
					// a candidate has already been given the first preference
					return false;
				}
			}
			
			// if the preference exceeds the number of candidates then the vote is not formal
			if (pref > this.getNumCandidates()) {
				return false;
			}
		}
		
		// only valid vote if the first_pref_exists
		return (first_pref_exists);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = this.name + " - Simple Voting";
		return str;
	}
	
	// Protected and Private/helper methods below///

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Election#clearWinner(int)
	 */
	@Override
	protected Candidate clearWinner(int wVotes) {
		// count the primary votes
		//vc.countPrimaryVotes(cds);
		
		/* find the candidate with the most first preference votes */
		// setup the keys for the candidates collection
		Set<CandidateIndex> candidates = cds.keySet();
		
		// setup variables to remember who has the highest vote count
		CandidateIndex winnerIndex = null;
		
		// iterate over all candidates
		for (CandidateIndex cdi : candidates) {
			// if this candidate has more votes than the last most successful
			if (cds.get(cdi).getVoteCount() > wVotes) {
				winnerIndex = cdi;
			}
			
		}
		
		if (winnerIndex != null) {
			return cds.get(winnerIndex);
		}
		else {
			return null;
		}

	}

	/**
	 * Helper method to create a string reporting the count result
	 * 
	 * @return <code>String</code> containing summary of the count
	 */
	private String reportCountResult() {
		String str = "\nSimple election: " + this.name + "\n\n"
				+ candidateVoteSummary() + "\n";
		String inf = "Informal";
		String voteStr = "" + this.vc.getInformalCount();
		int length = ElectionManager.DisplayFieldWidth - inf.length()
				- voteStr.length();
		str += inf + Strings.createPadding(' ', length) + voteStr + "\n\n";

		String cast = "Votes Cast";
		voteStr = "" + this.numVotes;
		length = ElectionManager.DisplayFieldWidth - cast.length()
				- voteStr.length();
		str += cast + Strings.createPadding(' ', length) + voteStr + "\n\n";
		return str;
	}
}