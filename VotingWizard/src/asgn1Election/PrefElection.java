/**
 * 
 * This file is part of the VotingWizard Project, written as 
 * part of the assessment for CAB302, Semester 1, 2016. 
 * 
 */
package asgn1Election;

import java.util.BitSet;
import java.util.Iterator; // jake imported this
import java.util.Map.Entry;

import asgn1Util.Strings;

/**
 * 
 * Subclass of <code>Election</code>, specialised to preferential, but not optional
 * preferential voting.
 * 
 * @author hogan
 * 
 */
public class PrefElection extends Election {

	/**
	 * Simple Constructor for <code>PrefElection</code>, takes name and also sets the 
	 * election type internally. 
	 * 
	 * @param name <code>String</code> containing the Election name
	 */
	public PrefElection(String name) {
		super(name);
		this.type = 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Election#findWinner()
	 */
	@Override
	public String findWinner() {
		// the string to output
		String output = "";
		
		output += showResultHeader();
		output += "Counting primary votes; " + cds.size() + " alternatives available\n";
		
		vc.countPrimaryVotes(this.cds);
		
		output += reportCountStatus();
		
		// while there isnt a clearWinner
		while (clearWinner(vc.getFormalCount() / 2 + 1) == null) {
			// find the lowest voted candidate
			CandidateIndex lowest = this.selectLowestCandidate();
			output += prefDistMessage(cds.get(lowest)) + "\n";
			
			// distribute the votes from the lowest candidate and eliminate them
			vc.countPrefVotes(this.cds, lowest);
			
			output += reportCountStatus();
			
		}
		
		Candidate winner = this.clearWinner(vc.getFormalCount() / 2 + 1);
		output += this.reportWinner(winner);

		return output;
	}

	/* 
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Election#isFormal(asgn1Election.Vote)
	 */
	@Override
	public boolean isFormal(Vote v) {
		
		// parseVoteFromLine() already checks whether the number of integer entries ...
		// on the vote card is the same as the number of candidates
		
		// algorithm: check whether the i'th preference exists in the vote; where  1 <= i <= numCandidates
		
		// for the number of candidates (which should be the number of preferences)
		for (int i = 1; i <= this.numCandidates; i++) {
			
			// check the pref num. exists in the vote 
			Iterator<Integer> itr = v.iterator();
			boolean pref_exists = false;
			while (itr.hasNext()) {
				Integer pref = (Integer)itr.next();
				if (pref.equals(i)) {
					// yes, the i'th preference does exist!
					pref_exists = true;
				}
			}
			
			if (!pref_exists) {
				// the i'th preference did not exist, this cannot be a valid vote!
				return false;
			}
			
		}
		
		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
        String str = this.name + " - Preferential Voting";
		return str;
	}
	
	// Protected and Private/helper methods below///


	/*
	 * (non-Javadoc)
	 * 
	 * @see asgn1Election.Election#clearWinner(int)
	 */
	@Override
	protected Candidate clearWinner(int winVotes) {
		// check whether a candidate has greater/equal number of votes than winVotes
		for(Entry<CandidateIndex, Candidate> entry : cds.entrySet()) {
			  Candidate cd = entry.getValue();
			  if (cd.getVoteCount() >= winVotes) {
				  return cd;
			  }
		}
		return null;
	}

	/**
	 * Helper method to create a preference distribution message for display 
	 * 
	 * @param c <code>Candidate</code> to be eliminated
	 * @return <code>String</code> containing preference distribution message 
	 */
	private String prefDistMessage(Candidate c) {
		String str = "\nPreferences required: distributing " + c.getName()
				+ ": " + c.getVoteCount() + " votes";
		return str;
	}

	/**
	 * Helper method to create a string reporting the count progress
	 * 
	 * @return <code>String</code> containing count status  
	 */
	private String reportCountStatus() {
		String str = "\nPreferential election: " + this.name + "\n\n"
				+ candidateVoteSummary() + "\n";
		String inf = "Informal";
		String voteStr = "" + this.vc.getInformalCount();
		int length = ElectionManager.DisplayFieldWidth - inf.length()
				- voteStr.length();
		str += inf + Strings.createPadding(' ', length) + voteStr + "\n\n";

		String cast = "Votes Cast";
		voteStr = "" + this.numVotes;
		length = ElectionManager.DisplayFieldWidth - cast.length()
				- voteStr.length();
		str += cast + Strings.createPadding(' ', length) + voteStr + "\n\n";
		return str;
	}

	/**
	 * Helper method to select candidate with fewest votes
	 * 
	 * @return <code>CandidateIndex</code> of candidate with fewest votes
	 */
	private CandidateIndex selectLowestCandidate() {
		// assume the first candidate has the lowest number of votes
		CandidateIndex smlCandidateIndex = cds.firstKey();
		
		// check all the candidates against the current smlCandidateIndex
		for(Entry<CandidateIndex, Candidate> entry : cds.entrySet()) {
		    CandidateIndex current_cd = entry.getKey();
		    
		    int current_cd_vt = cds.get(current_cd).getVoteCount();
		    int sml_cnd_vt = cds.get(smlCandidateIndex).getVoteCount();
		    
		    if (current_cd_vt < sml_cnd_vt) {
		    	// the candidate being checked currently has even less votes
		    	smlCandidateIndex = current_cd;
		    }
		    
		}
		
		return smlCandidateIndex;
	}
}