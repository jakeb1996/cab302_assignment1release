/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.Candidate;
import asgn1Election.ElectionException;

/**
 * @author Jake
 *
 */
public class CandidateTests {

	private Candidate cd;
	
	/**
	 * Setup method
	 * @throws ElectionException 
	 */
	@Before
	public void testCandidateSetup() throws ElectionException {
		cd = new Candidate("CandName", "PartyName", "PartyAbbrev", 0);
	}
	
	/**
	 * Test constructor for a perfect scenario
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test
	public void testCandidateNoException() throws ElectionException {
		/* Tests the constructor - should not fail */
		new Candidate("CandName", "PartyName", "PartyAbbrev", 0);
	}

	/**
	 * Test the constructor for when voteCount is less than zero
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateVoteCountLessThanZero() throws ElectionException {
		/* Tests the constructor when the count is less than zero */
		new Candidate("CandName", "PartyName", "PartyAbbrev", -1);
	}
	
	/**
	 * Test the constructor for when the candidate name is null
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateNullCandidateName() throws ElectionException {
		new Candidate(null, "PartyName", "PartyAbbrev", 0);
	}
	
	/**
	 * Test the constructor for when the party name is null
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateNullPartyName() throws ElectionException {
		new Candidate("CandName", null, "PartyAbbrev", 0);
	}
	
	/**
	 * Test the constructor for when the party abbreviation is null
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateNullPartyAbbrev() throws ElectionException {
		new Candidate("CandName", "PartyName", null, 0);
	}

	/**
	 * Test the constructor for when the candidate name is empty
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateEmptyCandidateName() throws ElectionException {
		new Candidate("", "PartyName", "PartyAbbrev", 0);
	}
	
	/**
	 * Test the constructor for when the party name is empty
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateEmptyPartyName() throws ElectionException {
		new Candidate("CandName", "", "PartyAbbrev", 0);
	}
	
	/**
	 * Test the constructor for when the party abbreviation is empty
	 * Test method for {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws ElectionException 
	 */
	@Test(expected=asgn1Election.ElectionException.class)
	public void testCandidateEmptyPartyAbbrev() throws ElectionException {
		new Candidate("CandName", "PartyName", "", 0);
	}
	
	/**
	 * Test copy() to check a deep copy is actually made
	 * Test method for {@link asgn1Election.Candidate#copy()}.
	 * @throws ElectionException 
	 */
	@Test
	public void testCopy() throws ElectionException {
		Candidate copied = cd.copy();
		assertEquals(cd.toString(), copied.toString());
	}

	/**
	 * incrementVoteCount() once. Initially voteCount is zero, should be one after incrementing.
	 * Test method for {@link asgn1Election.Candidate#incrementVoteCount()}.
	 */
	@Test
	public void testIncrementVoteCount() {
		cd.incrementVoteCount();
		assertEquals(1, cd.getVoteCount());
	}

	/**
	 * incrementVoteCount() three times. Final voteCount should be three.
	 * Test method for {@link asgn1Election.Candidate#incrementVoteCount()}.
	 */
	@Test
	public void testIncrementVoteCountThreeTimes() {
		cd.incrementVoteCount();
		cd.incrementVoteCount();
		cd.incrementVoteCount();
		assertEquals(3, cd.getVoteCount());
	}
	
	/**
	 * candidateListing() not tested
	 * Test method for {@link asgn1Election.Candidate#candidateListing()}.
	 */
	@Test
	public void testCandidateListingNotTested() {
		return;
	}
	
	/**
	 * getName() not tested
	 * Test method for {@link asgn1Election.Candidate#getName()}.
	 */
	@Test
	public void testGetNameNotTested() {
		return;
	}
	
	/**
	 * getParty() not tested
	 * Test method for {@link asgn1Election.Candidate#getParty()}.
	 */
	@Test
	public void testGetPartyNotTested() {
		return;
	}
	
	/**
	 * getVoteCount() not tested
	 * Test method for {@link asgn1Election.Candidate#getVoteCount()}.
	 */
	@Test
	public void testGetVoteCountNotTested() {
		return;
	}
	
	/**
	 * getVoteCountString() not tested
	 * Test method for {@link asgn1Election.Candidate#getVoteCountString()}.
	 */
	@Test
	public void testGetVoteCountStringNotTested() {
		return;
	}
	
	/**
	 * toString() not tested
	 * Test method for {@link asgn1Election.Candidate#toString()}.
	 */
	@Test
	public void testToStringNotTested() {
		return;
	}
}
