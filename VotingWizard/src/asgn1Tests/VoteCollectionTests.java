package asgn1Tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.Candidate;
import asgn1Election.CandidateIndex;
import asgn1Election.Election;
import asgn1Election.ElectionException;
import asgn1Election.PrefElection;
import asgn1Election.VoteCollection;
import asgn1Election.VoteList;
import asgn1Util.NumbersException;

public class VoteCollectionTests {

	private VoteCollection vc;
	private TreeMap<CandidateIndex, Candidate> cds;
	
	@Before
	public void testVoteCollectionSetup() throws ElectionException, NoSuchFieldException, SecurityException {
		vc = new VoteCollection(3);
		cds = new TreeMap<CandidateIndex, Candidate>();
		cds.put(new CandidateIndex(1), new Candidate("cand1", "party1", "P1", 0));
		cds.put(new CandidateIndex(2), new Candidate("cand2", "party2", "P2", 0));
		cds.put(new CandidateIndex(3), new Candidate("cand3", "party3", "P3", 0));
	}
	
	/**
	 * Tests the constructor throws an exception when the numCandidates is below the allowable range
	 * Test method for {@link asgn1Election.VoteCollection#VoteCollection()}.
	 */
	@Test (expected=asgn1Election.ElectionException.class)
	public void testVoteCollectionUnderRange() throws ElectionException {
		new VoteCollection(-100000);
	}
	
	/**
	 * Tests the constructor when numCandidates is within the allowable range
	 * Test method for {@link asgn1Election.VoteCollection#VoteCollection()}.
	 */
	@Test
	public void testVoteCollectionWithinRange() throws ElectionException {
		new VoteCollection(5);
	}
	
	/**
	 * Tests the constructor throws an exception when the numCandidates is above the allowable range
	 * Test method for {@link asgn1Election.VoteCollection#VoteCollection()}.
	 */
	@Test (expected=asgn1Election.ElectionException.class)
	public void testVoteCollectionAboveRange() throws ElectionException {
		new VoteCollection(100000);
	}

	/**
	 * Tests that countPrefVotes() correctly distributes votes for the first time
	 * Test method for {@link asgn1Election.VoteCollection#countPrefVotes(TreeMap<CandidateIndex, Candidate> cds, CandidateIndex elim)}.
	 */
	@Test
	public void testCountPrefVotesOneCandidateEliminated() {
		// make a three votes
		// Candidate1 has two first preferred votes
		// Candidate3 has one first preferred votes
		// Candidate3 is eliminated
		VoteList v1 = new VoteList(3);
		v1.addPref(1); v1.addPref(2); v1.addPref(3);
		vc.includeFormalVote(v1);
		
		VoteList v2 = new VoteList(3);
		v2.addPref(1); v2.addPref(2); v2.addPref(3);
		vc.includeFormalVote(v2);
		
		VoteList v3 = new VoteList(3);
		v3.addPref(3); v3.addPref(2); v3.addPref(1);
		vc.includeFormalVote(v3);
				
		vc.countPrimaryVotes(cds);
		
		// Now, eliminate Candidate3
		vc.countPrefVotes(cds, new CandidateIndex(3));
		
		// the vote from Candidate3 is passed to Candidate2
		// check Candidate1 has two votes, Candidate2 has one vote and Candidate3 is removed
		
		Integer cd1_votes = ((Candidate)cds.get(new CandidateIndex(1))).getVoteCount();
		Integer cd2_votes = ((Candidate)cds.get(new CandidateIndex(2))).getVoteCount();
		Boolean cd3_exists = cds.containsKey(new CandidateIndex(3));
		
		assertTrue(cd1_votes == 2 && cd2_votes == 1 && !cd3_exists);
	}
	
	/**
	 * Tests that countPrefVotes() correctly distributes votes for a second time
	 * Test method for {@link asgn1Election.VoteCollection#countPrefVotes(TreeMap<CandidateIndex, Candidate> cds, CandidateIndex elim)}.
	 */
	@Test
	public void testCountPrefVotesTwoCandidatesEliminated() {
		// make a three votes
		// Candidate1 has two first preferred votes
		// Candidate3 has one first preferred votes
		// Candidate3 is eliminated
		VoteList v1 = new VoteList(3);
		v1.addPref(1); v1.addPref(2); v1.addPref(3);
		vc.includeFormalVote(v1);
		
		VoteList v2 = new VoteList(3);
		v2.addPref(1); v2.addPref(2); v2.addPref(3);
		vc.includeFormalVote(v2);
		
		VoteList v3 = new VoteList(3);
		v3.addPref(3); v3.addPref(2); v3.addPref(1);
		vc.includeFormalVote(v3);
				
		vc.countPrimaryVotes(cds);
		
		// Now, eliminate Candidate3
		vc.countPrefVotes(cds, new CandidateIndex(3));
		
		// Candidate1 has two votes, Candidate2 has one vote and Candidate3 is removed
		// Eliminate Candidate2. The vote from Candidate2 goes to Candidate1.
		vc.countPrefVotes(cds, new CandidateIndex(2));
		
		// check Candidate1 has three votes, Candidate2 and Candidate3 are removed
		Integer cd1_votes = ((Candidate)cds.get(new CandidateIndex(1))).getVoteCount();
		Boolean cd2_exists = cds.containsKey(new CandidateIndex(2));
		Boolean cd3_exists = cds.containsKey(new CandidateIndex(3));
		
		assertTrue(cd1_votes == 3 && !cd2_exists && !cd3_exists);
	}
	
	/**
	 * Tests that countPrimaryVotes() correctly counts first preference votes
	 * Test method for {@link asgn1Election.VoteCollection#countPrimaryVotes(TreeMap<CandidateIndex, Candidate> cds)}.
	 */
	@Test
	public void testCountPrimaryVotesOneEach() {
		// make a three votes so that each candidate has one first preference vote
		VoteList v1 = new VoteList(3);
		v1.addPref(1); v1.addPref(2); v1.addPref(3);
		vc.includeFormalVote(v1);
		
		VoteList v2 = new VoteList(3);
		v2.addPref(2); v2.addPref(1); v2.addPref(3);
		vc.includeFormalVote(v2);
		
		VoteList v3 = new VoteList(3);
		v3.addPref(3); v3.addPref(2); v3.addPref(1);
		vc.includeFormalVote(v3);
		
		vc.countPrimaryVotes(cds);
		
		Integer cd1 = ((Candidate)cds.get(new CandidateIndex(1))).getVoteCount();
		Integer cd2 = ((Candidate)cds.get(new CandidateIndex(2))).getVoteCount();
		Integer cd3 = ((Candidate)cds.get(new CandidateIndex(3))).getVoteCount();
		
		assertTrue(cd1 == 1 && cd2 == 1 && cd3 == 1);

	}

	/**
	 * emptyTheCollection() is a setter. It is assumed that ArrayList.clear() functions as needed.
	 * Test method for {@link asgn1Election.VoteCollection#emptyTheCollection()}.
	 */
	@Test
	public void testEmptyTheCollection() {
		return;
	}

	/**
	 * Tests that UpdateFormalCount() increments FormalCount by 1
	 * Test method for {@link asgn1Election.VoteCollection#IncludeFormalCount()}.
	 */
	@Test
	public void testIncludeFormalVote() {
		VoteList vt = new VoteList(3);
		vc.includeFormalVote(vt);
		assertEquals(1, vc.getFormalCount());
		
		//https://www.facebook.com/groups/197170030647118/permalink/229977630699691/?comment_id=229978507366270&reply_comment_id=229992717364849&comment_tracking=%7B%22tn%22%3A%22R9%22%7D
		
	}
	
	/**
	 * Tests that UpdateFormalCount() increments FormalCount by 2
	 * Test method for {@link asgn1Election.VoteCollection#IncludeFormalCount()}.
	 */
	@Test
	public void testIncludeFormalVoteTwice() {
		VoteList vt = new VoteList(3);
		vc.includeFormalVote(vt);
		
		VoteList vt2 = new VoteList(3);
		vc.includeFormalVote(vt2);
		
		assertEquals(2, vc.getFormalCount());
		
		//https://www.facebook.com/groups/197170030647118/permalink/229977630699691/?comment_id=229978507366270&reply_comment_id=229992717364849&comment_tracking=%7B%22tn%22%3A%22R9%22%7D
		
	}
	
	/**
	 * Tests that UpdateInformalCount() increments informalCount by 1
	 * Test method for {@link asgn1Election.VoteCollection#UpdateInformalCount()}.
	 */
	@Test
	public void testUpdateInformalCount() {
		vc.updateInformalCount();
		assertEquals(1, vc.getInformalCount());
	}
	
	/**
	 * Tests that running UpdateInformalCount() three times makes informalCount equal three
	 * Test method for {@link asgn1Election.VoteCollection#UpdateInformalCount()}.
	 */
	@Test
	public void testUpdateInformalCountThreeTimes() {
		vc.updateInformalCount();
		vc.updateInformalCount();
		vc.updateInformalCount();
		assertEquals(3, vc.getInformalCount());
	}

	/**
	 * getInformalCount() not tested
	 * Test method for {@link asgn1Election.VoteCollection#getInformalCount()}.
	 */
	@Test
	public void testGetInformalCountNotTested() {
		return;
	}
	
	/**
	 * getFormalCount() not tested
	 * Test method for {@link asgn1Election.VoteCollection#getFormalCount()}.
	 */
	@Test
	public void testGetFormalCountNotTested() {
		return;
	}
}
