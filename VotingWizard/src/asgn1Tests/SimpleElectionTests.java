package asgn1Tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.ElectionException;
import asgn1Election.SimpleElection;
import asgn1Election.VoteList;
import asgn1Util.NumbersException;

public class SimpleElectionTests {

	private VoteList vt;
	private SimpleElection elc;
	
	@Before
	public void testSimpleElectionTestsSetup() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		vt = new VoteList(5);
		elc = new SimpleElection("MorgulVale");
		elc.loadDefs();
		elc.loadVotes();
	}
	
	/**
	 * Test FindWinner() with MorgulVale dataset
	 * Test method for {@link asgn1Election.SimpleElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMorgulVale() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		SimpleElection election = new SimpleElection("MorgulValeSimple");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MorgulValeSimple\r\n" + 
				"Enrolment: 83483\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"Black Rider         Nazgul Party                  (NP)\r\n" + 
				"Mouth of Sauron     Whatever Sauron Says Party    (WSSP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 5 alternatives available\r\n" + 
				"\r\n" + 
				"Simple election: MorgulValeSimple\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                10\r\n" + 
				"Gorbag (FOP)                 5\r\n" + 
				"Shagrat (SOP)                4\r\n" + 
				"Black Rider (NP)             9\r\n" + 
				"Mouth of Sauron (WSSP)       3\r\n" + 
				"\r\n" + 
				"Informal                     0\r\n" + 
				"\r\n" + 
				"Votes Cast                  31\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Shelob (Monster Spider Party) is the winner with 10 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}
	
	/**
	 * Test FindWinner() with MinMorgulVale dataset
	 * Test method for {@link asgn1Election.SimpleElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMinMorgulVale() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		SimpleElection election = new SimpleElection("MinMorgulValeSimple");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MinMorgulValeSimple\r\n" + 
				"Enrolment: 25\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 3 alternatives available\r\n" + 
				"\r\n" + 
				"Simple election: MinMorgulValeSimple\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 8\r\n" + 
				"Gorbag (FOP)                 8\r\n" + 
				"Shagrat (SOP)                3\r\n" + 
				"\r\n" + 
				"Informal                     4\r\n" + 
				"\r\n" + 
				"Votes Cast                  23\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Shelob (Monster Spider Party) is the winner with 8 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}
	
	/**
	 * Test FindWinner() with MinMorgulValeTie dataset
	 * Test method for {@link asgn1Election.SimpleElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMinMorgulValeTie() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		SimpleElection election = new SimpleElection("MinMorgulValeTieSimple");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MinMorgulValeTieSimple\r\n" + 
				"Enrolment: 25\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 3 alternatives available\r\n" + 
				"\r\n" + 
				"Simple election: MinMorgulValeTieSimple\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 8\r\n" + 
				"Gorbag (FOP)                 7\r\n" + 
				"Shagrat (SOP)                3\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Shelob (Monster Spider Party) is the winner with 8 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}

	/**
	 * A SimpleElection vote is valid for preferences 1 to numCandidates (ascending)
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormal1To5() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		
		assertTrue(elc.isFormal(vt));
	}

	/**
	 * A SimpleElection vote is valid for preferences 1 to numCandidates (any order)
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormal1To5AnyOrder() {
		vt.addPref(2);
		vt.addPref(1);
		vt.addPref(3);
		vt.addPref(5);
		vt.addPref(4);
		
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is valid even when the second preference is repeating
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalFirstPrefAndMultipleSecondPrefs() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(2);
		vt.addPref(2);
		vt.addPref(2);
		
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is valid even when the third preference is repeating
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalFirstPrefAndMultipleThirdPrefs() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(3);
		vt.addPref(5);
		
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is valid even when the second preference is missing
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalFirstPrefSkipSecond() {
		vt.addPref(1);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		vt.addPref(4);
		
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is invalid when the first preference is listed multiple times
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalMultipleFirstPreferences() {
		vt.addPref(1);
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is invalid when a candidate has a preference greater than numCandidates
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalFirstPrefAndOutOfBoundsPref() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(10000);
		
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * A SimpleElection vote is invalid when the first preference is missing
	 * Test method for {@link asgn1Election.SimpleElection#isFormal()}. 
	 */
	@Test
	public void testIsFormalMissingFirstPref() {
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		vt.addPref(4);
		
		assertFalse(elc.isFormal(vt));
	}


}
