/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.CandidateIndex;

/**
 * @author Jake
 *
 */
public class CandidateIndexTests {

	private CandidateIndex cdi;
	
	/*
	 * Setup for CandidateIndex tests
	 */
	@Before
	public void testCandidateIndexSetup() {
		cdi = new CandidateIndex(CandidateIndex.MaxCandidates - (CandidateIndex.MaxCandidates - CandidateIndex.MinCandidates) / 2);
	}
	
	/**
	 * Test inRange() when value is lower than MinCandidate
	 * Test method for {@link asgn1Election.CandidateIndex#inRange(int)}.
	 */
	@Test
	public void testInRangeBelowMinimum() {
		assertFalse(CandidateIndex.inRange(CandidateIndex.MinCandidates - 1));
	}
	
	/**
	 * Test inRange() when value is within range
	 * Test method for {@link asgn1Election.CandidateIndex#inRange(int)}.
	 */
	@Test
	public void testInRangeWithinRange() {
		assertTrue(CandidateIndex.inRange(CandidateIndex.MaxCandidates - CandidateIndex.MinCandidates));
	}

	
	/**
	 * Test inRange() when value is higher than MaxCandidate
	 * Test method for {@link asgn1Election.CandidateIndex#inRange(int)}.
	 */
	@Test
	public void testInRangeAboveMaximum() {
		assertEquals(false, CandidateIndex.inRange(CandidateIndex.MaxCandidates + 1));
	}

	/**
	 * Test CompareTo() a CandidateIndex less than current CandidateIndex
	 * Test method for {@link asgn1Election.CandidateIndex#compareTo(asgn1Election.CandidateIndex)}.
	 */
	@Test
	public void testCompareToCandidateIndexLessThan() {
		CandidateIndex cdi2 = new CandidateIndex(CandidateIndex.MinCandidates);
		assertEquals(-1, cdi2.compareTo(cdi));
	}
	
	/**
	 * Test CompareTo() a CandidateIndex same value as current CandidateIndex
	 * Test method for {@link asgn1Election.CandidateIndex#compareTo(asgn1Election.CandidateIndex)}.
	 */
	@Test
	public void testCompareToCandidateIndexSame() {
		assertEquals(0, cdi.compareTo(cdi));
	}
	
	/**
	 * Test CompareTo() a CandidateIndex greater than current CandidateIndex
	 * Test method for {@link asgn1Election.CandidateIndex#compareTo(asgn1Election.CandidateIndex)}.
	 */
	@Test
	public void testCompareToCandidateIndexGreaterThan() {
		CandidateIndex cdi2 = new CandidateIndex(CandidateIndex.MaxCandidates);
		assertEquals(1, cdi2.compareTo(cdi));
	}

	/**
	 * Test Copy() to achieve a proper full deep copy
	 * Test method for {@link asgn1Election.CandidateIndex#copy()}.
	 */
	@Test
	public void testCopy() {
		CandidateIndex copied = cdi.copy();
		assertEquals(cdi.toString(), copied.toString());
	}

	/**
	 * Test IncrementIndex() of CandidateIndex only once
	 * Test method for {@link asgn1Election.CandidateIndex#incrementIndex()}.
	 */
	@Test
	public void testIncrementIndex() {
		CandidateIndex incremented = cdi.copy();
		incremented.incrementIndex();
		assertEquals(1, incremented.compareTo(cdi));
	}
	
	/**
	 * Test IncrementIndex() for multiple increments
	 * Test method for {@link asgn1Election.CandidateIndex#incrementIndex()}.
	 */
	@Test
	public void testIncrementIndexThreeTimes() {
		CandidateIndex incremented = cdi.copy();
		incremented.incrementIndex();
		incremented.incrementIndex();
		incremented.incrementIndex();
		assertEquals(1, incremented.compareTo(cdi));
	}
	
	/**
	 * CandidateListing() not tested.
	 * Test method for {@link asgn1Election.CandidateIndex#candidateListing()}.
	 */
	@Test
	public void testCandidateListingNotTested() {
		return;
	}
	
	/**
	 * SetValue() not tested.
	 * Test method for {@link asgn1Election.CandidateIndex#setValue()}.
	 */
	@Test
	public void testSetValueNotTested() {
		return;
	}
	
}
