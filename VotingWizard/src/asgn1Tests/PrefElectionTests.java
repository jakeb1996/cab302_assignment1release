/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.Candidate;
import asgn1Election.CandidateIndex;
import asgn1Election.Election;
import asgn1Election.ElectionException;
import asgn1Election.Vote;
import asgn1Election.VoteCollection;
import asgn1Election.VoteList;
import asgn1Util.NumbersException;

/**
 * @author Jake
 *
 */
public class PrefElectionTests {

	private ExtendedElection elc;
	private Vote vt;
	
	@Before
	public void setupPrefElectionTests() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		elc = new ExtendedElection("MorgulVale");
		elc.loadDefs();
		elc.loadVotes();
		vt = new VoteList(5);
	}


	/**
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testLoadDefsCorrectlyLoadsCandidates() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		// Manually generate the candidates
		TreeMap<Integer, Candidate> manualCDS = new TreeMap<Integer, Candidate>();
		manualCDS.put(0, new Candidate("Shelob", "Monster Spider Party", "MSP", 0));
		manualCDS.put(1, new Candidate("Gorbag", "Filthy Orc Party", "FOP", 0));
		manualCDS.put(2, new Candidate("Shagrat", "Stinking Orc Party", "SOP", 0));
		manualCDS.put(3, new Candidate("Black Rider", "Nazgul Party", "NP", 0));
		manualCDS.put(4, new Candidate("Mouth of Sauron", "Whatever Sauron Says Party", "WSSP", 0));
		
		// Compare the manually generated cand's to the auto-gen' cand's
		String manualCDSoutput = manualCDS.values().toString();
		String MorgulValeOutput = elc.getCandidates().toString();
		assertTrue(manualCDSoutput.equals(MorgulValeOutput));
	}
	
	
	/**
	 * Test loadDefs() for when electorate summary is missing from ote file
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsMissingElectorateSummaryString() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcMissingElectorateSummary");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when seat name is missing from elc file
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsSeatNameIsMissing() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcSeatNameIsMissing");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when enrolment count is missing from elc file
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsEnrolmentIsMissing() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcEnrolmentIsMissing");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when numCandidates is missing from elc file
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsNumCandidatesIsMissing() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcNumCandidatesMissing");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when enrolment number is a character
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Util.NumbersException.class)
	public void testLoadDefsEnrolmentIsACharacter() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcEnrolmentIsACharacter");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when numCandidates is a character
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Util.NumbersException.class)
	public void testLoadDefsNumCandidatesIsACharacter() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcNumCandidatesIsACharacter");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when numCandidates is greater than candidates supplied
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsNumCandidatesGreaterThanActual() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcNumCandidatesGreaterThanActual");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when a candidate attribute is missing
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsCandidateMissingToken() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcCandidateMissingToken");
		election.loadDefs();
	}
	
	/**
	 * Test loadDefs() for when a candidate attribute is empty
	 * Test method for {@link asgn1Election.PrefElection#loadDefs()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadDefsCandidateTokenEmpty() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testElcCandidateTokenEmpty");
		election.loadDefs();
	}
		
	/**
	 * Test loadVotes() for when the vote line is empty
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadVotesEmptyLine() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtEmptyLine");
		election.loadVotes();
	}
	
	/**
	 * Test loadVotes() for when there is a different number of preferences in a vote than number of candidates
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadVotesDiffNumTokensThanNumCands() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtDiffNumTokensThanNumCands");
		election.loadVotes();
	}
	
	/**
	 * Test loadVotes() for when a preference in a vote is empty
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Election.ElectionException.class)
	public void testLoadVotesEmptyVoteToken() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtEmptyVoteToken");
		election.loadVotes();
	}
	
	/**
	 * Test loadVotes() for when a preference is a non-integer-compatible character
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test (expected = asgn1Util.NumbersException.class)
	public void testLoadVotesPreferenceIsACharacter() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtPrefIsAChar");
		election.loadDefs();
		election.loadVotes();
	}
	
	
	/**
	 * Test loadVotes() for when there are ten votes in the vote file
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 *
	 */
	@Test
	public void testLoadVotesTenVotes() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtTenVotes");
		election.loadDefs();
		election.loadVotes();
		VoteCollection vc = (VoteCollection) election.getVoteCollection();
		
		assertEquals(10, vc.getFormalCount() + vc.getInformalCount());
	}
	
	/**
	 * Tests loadVotes() for when there are no votes in the vote file
	 * Test method for {@link asgn1Election.PrefElection#loadVotes()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 *
	 */
	@Test
	public void testLoadVotesZeroVotes() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("testVtZeroVotes");
		election.loadDefs();
		election.loadVotes();
		VoteCollection vc = (VoteCollection) election.getVoteCollection();
		
		assertEquals(0, vc.getFormalCount());
	}
	
	/**
	 * Test LoadDefs() for perfect case
	 * Test method for {@link asgn1Election.Election#loadDefs()()}.
	 */
	@Test
	public void testLoadDefsCorrectData() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		elc = new ExtendedElection("MinMorgulVale");
		elc.loadDefs();
	}
	
	/**
	 * Test LoadVotes() for 30 formal votes
	 * Test method for {@link asgn1Election.Election#loadVotes()()}.
	 */
	@Test
	public void testLoadVotesMorgulValeThirtyVotes() {
		VoteCollection vc = (VoteCollection) elc.getVoteCollection(); 
		assertTrue((vc.getFormalCount()==30) && (vc.getInformalCount()==0));
	}
	
	/**
	 * Test FindWinner() with MorgulVale dataset
	 * Test method for {@link asgn1Election.PrefElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMorgulVale() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("MorgulVale");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MorgulVale\r\n" + 
				"Enrolment: 83483\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"Black Rider         Nazgul Party                  (NP)\r\n" + 
				"Mouth of Sauron     Whatever Sauron Says Party    (WSSP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 5 alternatives available\r\n" + 
				"\r\n" + 
				"Preferential election: MorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 9\r\n" + 
				"Gorbag (FOP)                 5\r\n" + 
				"Shagrat (SOP)                4\r\n" + 
				"Black Rider (NP)             9\r\n" + 
				"Mouth of Sauron (WSSP)       3\r\n" + 
				"\r\n" + 
				"Informal                     0\r\n" + 
				"\r\n" + 
				"Votes Cast                  30\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Mouth of Sauron: 3 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 9\r\n" + 
				"Gorbag (FOP)                 5\r\n" + 
				"Shagrat (SOP)                6\r\n" + 
				"Black Rider (NP)            10\r\n" + 
				"\r\n" + 
				"Informal                     0\r\n" + 
				"\r\n" + 
				"Votes Cast                  30\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Gorbag: 5 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                12\r\n" + 
				"Shagrat (SOP)                7\r\n" + 
				"Black Rider (NP)            11\r\n" + 
				"\r\n" + 
				"Informal                     0\r\n" + 
				"\r\n" + 
				"Votes Cast                  30\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Shagrat: 7 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                14\r\n" + 
				"Black Rider (NP)            16\r\n" + 
				"\r\n" + 
				"Informal                     0\r\n" + 
				"\r\n" + 
				"Votes Cast                  30\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Black Rider (Nazgul Party) is the winner with 16 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}
	
	/**
	 * Test FindWinner() with MinMorgulVale dataset
	 * Test method for {@link asgn1Election.PrefElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMinMorgulVale() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("MinMorgulVale");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MinMorgulVale\r\n" + 
				"Enrolment: 25\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 3 alternatives available\r\n" + 
				"\r\n" + 
				"Preferential election: MinMorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 8\r\n" + 
				"Gorbag (FOP)                 7\r\n" + 
				"Shagrat (SOP)                3\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Shagrat: 3 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MinMorgulVale\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                10\r\n" + 
				"Gorbag (FOP)                 8\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Shelob (Monster Spider Party) is the winner with 10 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}
	
	/**
	 * Test FindWinner() with MinMorgulValeTie dataset
	 * Test method for {@link asgn1Election.PrefElection#findWinner()}.
	 * @throws NumbersException 
	 * @throws IOException 
	 * @throws ElectionException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testFindWinnerMinMorgulValeTie() throws FileNotFoundException, ElectionException, IOException, NumbersException {
		ExtendedElection election = new ExtendedElection("MinMorgulValeTie");
		election.loadDefs();
		election.loadVotes();
		//election.findWinner();
		
		String actualResult = "Results for election: MinMorgulValeTie\r\n" + 
				"Enrolment: 25\r\n" + 
				"\r\n" + 
				"Shelob              Monster Spider Party          (MSP)\r\n" + 
				"Gorbag              Filthy Orc Party              (FOP)\r\n" + 
				"Shagrat             Stinking Orc Party            (SOP)\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Counting primary votes; 3 alternatives available\r\n" + 
				"\r\n" + 
				"Preferential election: MinMorgulValeTie\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 8\r\n" + 
				"Gorbag (FOP)                 7\r\n" + 
				"Shagrat (SOP)                3\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Shagrat: 3 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MinMorgulValeTie\r\n" + 
				"\r\n" + 
				"Shelob (MSP)                 9\r\n" + 
				"Gorbag (FOP)                 9\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Preferences required: distributing Shelob: 9 votes\r\n" + 
				"\r\n" + 
				"Preferential election: MinMorgulValeTie\r\n" + 
				"\r\n" + 
				"Gorbag (FOP)                18\r\n" + 
				"\r\n" + 
				"Informal                     3\r\n" + 
				"\r\n" + 
				"Votes Cast                  21\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Candidate Gorbag (Filthy Orc Party) is the winner with 18 votes...";
		
		actualResult = actualResult.replaceAll("\n", "");
		actualResult = actualResult.replaceAll("\r", "");
		actualResult = actualResult.replaceAll(" ",  "");
		
		
		
		String generatedResult = election.findWinner();
		generatedResult = generatedResult.replaceAll("\n", "");
		generatedResult = generatedResult.replaceAll("\r", "");
		generatedResult = generatedResult.replaceAll(" ",  "");
		
		
		assertEquals(actualResult, generatedResult);
	}

	/**
	 * Test isFormal() for when preferences are in ascending order
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormal1toN() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when preferences are in any order between 1 and 5, ascending
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormal1toNAnyOrder() {
		vt.addPref(1);
		vt.addPref(4);
		vt.addPref(3);
		vt.addPref(2);
		vt.addPref(5);
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when a perfect vote is made in descending order
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalNto1() {
		vt.addPref(5);
		vt.addPref(4);
		vt.addPref(3);
		vt.addPref(2);
		vt.addPref(1);
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when a perfect vote is made with preferences in mixed descending order between 5 and 1
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalNto1AnyOrder() {
		vt.addPref(5);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(2);
		vt.addPref(1);
		assertTrue(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when there is two first preferences
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalDoubleFirstPref() {
		vt.addPref(1);
		vt.addPref(1);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when there is two second preferences
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalDoubleSecondPref() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(2);
		vt.addPref(4);
		vt.addPref(5);
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when there is no first preference
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalMissingFirstPref() {
		vt.addPref(2);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(5);
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when a candidates preference is greater than the number of candidates
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalPrefAboveBounds() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(3);
		vt.addPref(4);
		vt.addPref(6);
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when a candidate has a preference of zero
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalPrefBelow1() {
		vt.addPref(1);
		vt.addPref(2);
		vt.addPref(0);
		vt.addPref(4);
		vt.addPref(5);
		assertFalse(elc.isFormal(vt));
	}
	
	/**
	 * Test isFormal() for when all candidates have first preference
	 * Test method for {@link asgn1Election.PrefElection#isFormal(asgn1Election.Vote)}.
	 */
	@Test
	public void testIsFormalAllOnes() {
		vt.addPref(1);
		vt.addPref(1);
		vt.addPref(1);
		vt.addPref(1);
		vt.addPref(1);
		assertFalse(elc.isFormal(vt));
	}

	/**
	 * toString() not tested
	 * Test method for {@link asgn1Election.PrefElection#toString()}.
	 */
	@Test
	public void testToStringNotTested() {
		return;
	}

	

}
