/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.CandidateIndex;
import asgn1Election.Vote;
import asgn1Election.VoteList;

/**
 * @author Jake
 *
 */
public class VoteListTests {

	private VoteList vtl;
	private static final int NUM_CANDIDATES = 5;
	
	
	@Before
	public void testVoteListSetup() {
		vtl = new VoteList(NUM_CANDIDATES);
	}
	
	/**
	 * Test method for {@link asgn1Election.VoteList#VoteList(int)}.
	 */
	@Test
	public void testVoteList() {
		
	}

	/**
	 * Test method for {@link asgn1Election.VoteList#addPref(int)}.
	 */
	@Test
	public void testAddPrefOnePreference() {
		assertTrue(vtl.addPref(1));
	}
	
	/**
	 * Test method for {@link asgn1Election.VoteList#addPref(int)}.
	 */
	@Test
	public void testAddPrefSameNumberOfCandidates() {
		for (int i = 1; i < NUM_CANDIDATES; i++) {
			vtl.addPref(i);
		}
		assertTrue(vtl.addPref(NUM_CANDIDATES));
	}
	
	/**
	 * Test method for {@link asgn1Election.VoteList#addPref(int)}.
	 */
	@Test
	public void testAddPrefOneExtraPreference() {
		for (int i = 1; i <= NUM_CANDIDATES; i++) {
			vtl.addPref(i);
		}
		assertFalse(vtl.addPref(NUM_CANDIDATES + 1));
	}
	
	/**
	 * Test method for {@link asgn1Election.VoteList#copyVote()}.
	 */
	@Test
	public void testCopyVote() {
		// fill up preferences
		for (int i = 1; i <= NUM_CANDIDATES; i++) {
			vtl.addPref(i);
		}
		
		// copy the vote
		Vote copied = vtl.copyVote();
		
		// check they are the same
		assertEquals(vtl.toString(), copied.toString());
	}

	/**
	 * Test method for {@link asgn1Election.VoteList#getPreference(int)}.
	 */
	@Test
	public void testGetPreference() {
		// The candidate number is correlated with the preference number
		for (int i = 1; i <= NUM_CANDIDATES; i++) {
			vtl.addPref(i);
		}
		
		// if we grab the third preference, it should be the third candidate
		assertEquals(new CandidateIndex(3).toString(), vtl.getPreference(3).toString());
	}

	/**
	 * Test method for {@link asgn1Election.VoteList#invertVote()}.
	 */
	@Test
	public void testInvertVote() {
		vtl.addPref(3); // candidate 1
		vtl.addPref(5); // candidate 2
		vtl.addPref(1); // candidate 3
		vtl.addPref(2); // candidate 4
		vtl.addPref(4); // candidate 5
		
		// now invert the vote
		Vote inverted = vtl.invertVote();
		
		// what we expect (read the candidate numbers in the order of the preferences and subtract one)
		String expected = "2 3 0 4 1 ";
		
		assertTrue(expected.equals(inverted.toString()));
	}

	/**
	 * Test method for {@link asgn1Election.VoteList#iterator()}.
	 */
	@Test
	public void testIterator() {
		Iterator<Integer> itr = vtl.iterator();
		assertNotNull(itr);
	}


}
